package qatestlab.task4.tests;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import qatestlab.task4.BaseTest;

/**
 * Created by alexander on 03/12/2017.
 */
public class MobileBrowserCheck extends BaseTest {
    @Test
    public void checkMobileVersion() {
        actions.checkMobileDisplay();
        log("Mobile version display check finished");
    }
}
