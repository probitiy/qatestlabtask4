package qatestlab.task4.tests;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import qatestlab.task4.BaseTest;
import qatestlab.task4.pages.ShopMainPage;

/**
 * Created by alexander on 02/12/2017.
 */
public class OrderProduct extends BaseTest{
    @Test
    public void orderProduct() {
        actions.orderProduct();
        log("Order creation finished");
    }

}
