package qatestlab.task4.tests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import qatestlab.task4.BaseTest;

/**
 * Created by alexander on 03/12/2017.
 */
public class DesktopBrowserCheck extends BaseTest{
    @Test
    public void checkDesktopVersion() {
        actions.checkDesktopDisplay();
        log("Desktop version display check finished");
    }
}
