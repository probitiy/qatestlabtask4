package qatestlab.task4;

import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import qatestlab.task4.pages.*;

/**
 * Created by alexander on 02/12/2017.
 */
public class GeneralActions {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
//
//    Автоматизировать следующий сценарий:
//    Часть А. Проверка открываемой версии магазина:
//            1. Перейти на главную страницу магазина.
//2. Удостовериться, что пользователю отображается корректная версия сайта (обычная
//                                                                                  версия сайта с использованием настольного браузера, мобильная версия сайта с
//                                                                                  использованием мобильного браузера.)
//    Часть Б. Оформление заказа в магазине:
//            1. Перейти на главную страницу магазина.
//2. Перейти к списку всех товаров воспользовавшись ссылкой «Все товары»
//            3. Открыть случайный товар из отображаемого списка.
//            4. Добавить товар в корзину.
//            5. В сплывающем окне нажать на кнопку «Перейти к оформлению» для перехода в
//    корзину пользователя.
//            6. Проверить, что в корзине отображается одна позиция, название и цена
//    добавленного товара соответствует значениям, которые отображались на странице
//    товара.
//7. Нажать на кнопку «Оформление заказа».
//            8. Заполнить поля Имя, Фамилия, E-mail (должно быть уникальным) и перейти к
//    следующему шагу оформления заказа.
//            9. Указать адрес, почтовый индекс, город доставки. Перейти к следующему шагу.
//            10. Оставить настройки доставки без изменений, перейти к шагу оплаты заказа.
//11. Выбрать любой способ оплаты. Отметить опцию «Я ознакомлен(а) и согласен(на) с
//    Условиями обслуживания.» Оформить заказ.
//            12. В открывшемся окне подтверждения заказа проверить следующие значения:
//            - Пользователю отображается сообщение «Ваш заказ подтвержден»
//            - В деталях заказа отображается одна позиция, название и цена товара
//    соответствует значениям, которые отображались на странице товара.
//            13. Вернуться на страницу товара и проверить изменения количества товара в наличии
//            (в блоке справа, на вкладке «Подробнее о товаре»): количество товара должно
//    уменьшиться на единицу.

    public GeneralActions(EventFiringWebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void checkMobileDisplay() {
        ShopMainPage shopMainPage = new ShopMainPage(driver);
        shopMainPage.open();

        shopMainPage.checkMobileVersion();
    }

    public void checkDesktopDisplay() {
        ShopMainPage shopMainPage = new ShopMainPage(driver);
        shopMainPage.open();

        shopMainPage.checkDesktopVersion();
    }

    public void orderProduct() {
        ShopMainPage shopMainPage = new ShopMainPage(driver);
        shopMainPage.open();
        shopMainPage.clickAllProductsLink();

        ShopProductsPage shopProductsPage = new ShopProductsPage(driver);
        shopProductsPage.clickOnFirstProduct();

        ProductShowPage productShowPage = new ProductShowPage(driver);
        String productName = productShowPage.getName();
        String productPrice = productShowPage.getPrice();
        productShowPage.clickOrderDetails();
        String productCountBeforeOrder = productShowPage.getProductQuantity();

        productShowPage.clickAddToCartButton();
        productShowPage.clickGotToCartLink();

        CartPage cartPage = new CartPage(driver);
        cartPage.checkQuantity();
        cartPage.clickMakeOrderButton();

        NewOrderPage newOrderPage = new NewOrderPage(driver);
        newOrderPage.clickPersonalInfoLink();
        newOrderPage.fillPersonalInfo();
        newOrderPage.fillAddressInfo();
        newOrderPage.fillDeliveryInfo();
        newOrderPage.fillPaymentInfo();

        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(driver);
        orderConfirmationPage.checkConfirmationText();
        orderConfirmationPage.checkProductKindsCount();
        orderConfirmationPage.checkProductDetails(productPrice, productName);

        shopMainPage.open();
        shopMainPage.clickAllProductsLink();
        shopProductsPage.searchProduct(productName);

        SearchProductPage searchProductPage = new SearchProductPage(driver);
        searchProductPage.openElement(productName);
        productShowPage.clickOrderDetails();
        String productCountAfterOrder = productShowPage.getProductQuantity();
        Assert.assertEquals(Integer.parseInt(productCountBeforeOrder) - 1,Integer.parseInt(productCountAfterOrder));
    }
}
