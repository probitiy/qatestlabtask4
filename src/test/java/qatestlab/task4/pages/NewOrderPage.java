package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import qatestlab.task4.models.PersonalInfoData;

/**
 * Created by alexander on 03/12/2017.
 */
public class NewOrderPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By firstNameField= By.name("firstname");
    private By lastNameField = By.name("lastname");
    private By emailField = By.name("email");
    private By addressField = By.name("address1");
    private By postcodeField = By.name("postcode");
    private By cityField = By.name("city");
    private By deiveryOptions = By.className("delivery-options-list");
    private By firstPaymentOption = By.id("payment-option-1");
    private By termsApproveCheckbox = By.name("conditions_to_approve[terms-and-conditions]");
    private By paymentOptions = By.className("payment-options");
    private By finishOrderButton = By.cssSelector("#payment-confirmation button.btn-primary");

    private By continueButton = By.cssSelector(".form-footer .continue.btn-primary");
    private By confirmDeliveryButton = By.name("confirmDeliveryOption");

    private By personalInformationSection = By.id("checkout-personal-information-step");
    private By addressSection = By.id("checkout-addresses-step");
    private By deliverySection = By.id("checkout-delivery-step");
    private By paymentSection = By.id("checkout-payment-step");

    private PersonalInfoData orderInfo;

    public NewOrderPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
        this.orderInfo = PersonalInfoData.generate();
    }

    public void clickPersonalInfoLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(personalInformationSection));
        driver.findElement(personalInformationSection).click();
    }

    public void fillPersonalInfo() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(firstNameField));
        driver.findElement(personalInformationSection).findElement(firstNameField).sendKeys(orderInfo.getFirstName());
        driver.findElement(personalInformationSection).findElement(lastNameField).sendKeys(orderInfo.getLastName());
        driver.findElement(personalInformationSection).findElement(emailField).sendKeys(orderInfo.getEmail());
        driver.findElement(personalInformationSection).findElement(continueButton).click();

    }

    public void fillAddressInfo() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(addressField));
        driver.findElement(addressSection).findElement(addressField).sendKeys(orderInfo.getAddress());
        driver.findElement(addressSection).findElement(cityField).sendKeys(orderInfo.getCity());
        driver.findElement(addressSection).findElement(postcodeField).sendKeys(orderInfo.getPostalCode());
        driver.findElement(addressSection).findElement(continueButton).click();
    }

    public void fillDeliveryInfo() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(deiveryOptions));
        driver.findElement(deliverySection).findElement(confirmDeliveryButton).click();
    }

    public void fillPaymentInfo() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(paymentOptions));
        driver.findElement(firstPaymentOption).click();
        driver.findElement(termsApproveCheckbox).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(finishOrderButton));
        driver.findElement(paymentSection).findElement(finishOrderButton).click();
    }

}
