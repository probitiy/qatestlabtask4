package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 03/12/2017.
 */
public class ProductShowPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By addToCartButton = By.cssSelector("button.add-to-cart");
    private By goToCartLink = By.cssSelector(".cart-content .btn-primary");
    private By productPrice = By.cssSelector(".product-price .current-price");
    private By productName = By.cssSelector("#content-wrapper [itemprop=name]");
    private By productDetails = By.cssSelector("[href=\"#product-details\"]");
    private By productQuantity = By.cssSelector(".product-quantities span");

    public ProductShowPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickAddToCartButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(addToCartButton));
        driver.findElement(addToCartButton).click();
    }

    public void clickGotToCartLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(goToCartLink));
        driver.findElement(goToCartLink).click();
    }

    public void clickOrderDetails() {
        driver.findElement(productDetails).click();
    }

    public String getProductQuantity() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productQuantity));
        return driver.findElement(productQuantity).getText().split(" ")[0];
    }

    public String getPrice() {
        return driver.findElement(productPrice).getText().split(" ")[0];
    }

    public String getName() {
        return driver.findElement(productName).getText();
    }



}
