package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by alexander on 03/12/2017.
 */
public class CartPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By quantityField= By.name("product-quantity-spin");
    private By makeOrderLink = By.cssSelector(".cart-detailed-actions .text-xs-center a");


    public CartPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void checkQuantity() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(quantityField));
        Assert.assertTrue(driver.findElement(quantityField).getAttribute("value").toString().equals("1"), "Quantity on cart is not equal to ordered");
    }

    public void clickMakeOrderButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(makeOrderLink));
        driver.findElement(makeOrderLink).click();
    }


}
