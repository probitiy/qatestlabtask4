package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by alexander on 03/12/2017.
 */
public class ShopMainPage {
    private String mainPageUrl = "http://prestashop-automation.qatestlab.com.ua/ru/";
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By allProductsLink = By.className("all-product-link");
    private By containerElement = By.className("container");
    private By mobileCartIcon = By.id("_mobile_cart");
    private By desktopCartIcon = By.id("_desktop_cart");

    public ShopMainPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void open() {
        driver.get(mainPageUrl);
    }

    public void clickAllProductsLink() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(allProductsLink));
        driver.findElement(allProductsLink).click();
    }

    public void checkMobileVersion() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(containerElement));
        Assert.assertTrue(driver.findElement(mobileCartIcon).isDisplayed());
        Assert.assertFalse(driver.findElement(desktopCartIcon).isDisplayed());
    }

    public void checkDesktopVersion() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(containerElement));
        Assert.assertFalse(driver.findElement(mobileCartIcon).isDisplayed());
        Assert.assertTrue(driver.findElement(desktopCartIcon).isDisplayed());
    }
}
