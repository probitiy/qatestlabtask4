package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 03/12/2017.
 */
public class ShopProductsPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By productTitleLink = By.cssSelector(".product-description .product-title");
    private By productSearchInput = By.className("ui-autocomplete-input");
    private By productSearchButton = By.cssSelector("#search_widget button[type=submit]");

    public ShopProductsPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickOnFirstProduct() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productTitleLink));

        driver.findElement(productTitleLink).click();
    }

    public void searchProduct(String productName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productSearchInput));
        driver.findElement(productSearchInput).sendKeys(productName);
        driver.findElement(productSearchButton).click();
    }
}
