package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by alexander on 03/12/2017.
 */
public class SearchProductPage {
    private WebDriverWait wait;
    private EventFiringWebDriver driver;
    private By foundProducts = By.cssSelector(".product-description .h3.product-title");
    private By productsList = By.cssSelector("#products");


    public SearchProductPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void openElement(String productName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(productsList));
        findElementWithByName(driver.findElements(foundProducts), productName).click();
    }

    private WebElement findElementWithByName(List<WebElement> elements, String name) {
        for(WebElement element: elements) {
            if (element.getText().toUpperCase().equals(name.toUpperCase())) {
                return element;
            }
        }
        return null;
    }
}
