package qatestlab.task4.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

/**
 * Created by alexander on 03/12/2017.
 */
public class OrderConfirmationPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By orderProductInfos= By.className("order-line");
    private By confirmationText = By.cssSelector("#content-hook_order_confirmation .card-title");
    private By priceField = By.xpath("//*[@id=\"order-items\"]//table[2]//tr[3]/td[2]");
    private By productNameField = By.cssSelector("#order-items .order-line .details");


    public OrderConfirmationPage(org.openqa.selenium.support.events.EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void checkConfirmationText() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(confirmationText));
        Assert.assertEquals(driver.findElement(confirmationText).getText(), "\uE876Ваш заказ подтверждён".toUpperCase());
    }

    public void checkProductKindsCount() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(orderProductInfos));
        Assert.assertEquals(driver.findElements(orderProductInfos).size(), 1);
    }

    public void checkProductDetails(String productPrice, String productName) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(priceField));
        Assert.assertEquals(driver.findElement(priceField).getText().split(" ")[0], productPrice);
        Assert.assertEquals(driver.findElement(productNameField).getText().split(" - Size :")[0].toUpperCase(), productName.toUpperCase());
    }
}
