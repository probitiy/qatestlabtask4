package qatestlab.task4;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import qatestlab.task4.utils.logging.EventHandler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;

/**
 * Created by alexander on 02/12/2017.
 */
public class BaseTest {
    protected EventFiringWebDriver driver;
    protected GeneralActions actions;

    private WebDriver getDriver(String browser) {
        if (browser.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") + "/drivers/geckodriver");
            return new FirefoxDriver();
        } else if (browser.equals("safari")) {
            return new SafariDriver();
        } else if (browser.equals("chrome_mobile")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
            Map<String, String> mobileEmulator = new HashMap<>();
            mobileEmulator.put("deviceName", "iPhone 6");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulator);
            return new ChromeDriver(chromeOptions);
        } else {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
            return new ChromeDriver();
        }
    }

    private RemoteWebDriver getRemoreDriver(String browser) throws MalformedURLException {
        DesiredCapabilities capabilities = null;
        if (browser.equals("firefox")) {
            capabilities = DesiredCapabilities.firefox();
        } else if (browser.equals("safari")) {
            capabilities = DesiredCapabilities.safari();
        } else if (browser.equals("chrome_mobile")) {
            Map<String, String> mobileEmulator = new HashMap<>();
            mobileEmulator.put("deviceName", "iPhone 6");
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulator);
            return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), chromeOptions);
        } else {
            capabilities = DesiredCapabilities.chrome();
        }
        return new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
    }


    @BeforeClass
    @Parameters("browser")
    public void setUp(String browser ) throws MalformedURLException {
        // Run Hub: java -jar grid/selenium-server-standalone-3.8.0.jar -role hub
        // Run Node: java -Dwebdriver.chrome.driver="drivers/chromedriver" -Dwebdriver.gecko.driver="drivers/geckodriver" -jar grid/selenium-server-standalone-3.8.0.jar -role node -hub http://localhost:4444/grid/register -browser "browserName=firefox,maxInstances=1" -browser "browserName=chrome,maxInstances=2"

//        driver = new EventFiringWebDriver(getRemoreDriver(browser));
        driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        Reporter.setEscapeHtml(false);

        actions = new GeneralActions(driver);
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    protected void log(String message) {
        Reporter.log(message + "<br/>");
    }
}
