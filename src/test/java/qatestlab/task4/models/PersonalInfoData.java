package qatestlab.task4.models;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Random;

/**
 * Created by alexander on 03/12/2017.
 */
public class PersonalInfoData {
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String postalCode;
    private String city;

    public PersonalInfoData(String firstName, String lastName, String email, String address, String postalCode, String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getEmail() {
        return email;
    }
    public String getAddress() {
        return address;
    }
    public String getPostalCode() {
        return postalCode;
    }
    public String getCity() {
        return city;
    }

    public static PersonalInfoData generate() {
        Random random = new Random();
        return new PersonalInfoData("John",
                                    "Doe",
                                    "johndoe" + (new Integer(random.nextInt(100) + 1)).toString() + "@example.com",
                                    "ул. Бережанская, 9",
                                    "44444",
                                    "Киев");
    }
}
